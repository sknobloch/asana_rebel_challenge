RSpec.describe LocationIq::Response do
  let(:response) { LocationIq::Response.new(plain_response) }

  context 'without error' do
    let(:plain_response) do
      '[{"place_id":"3762750","licence":"\u00a9 LocationIQ.com CC BY 4.0,' \
      'Data \u00a9 OpenStreetMap contributors, ODbL 1.0","osm_type":"node",' \
      '"osm_id":"417346627","boundingbox":["52.5074959","52.5075959",' \
      '"13.3903185","13.3904185"],"lat":"52.5075459","lon":"13.3903685",' \
      '"display_name":"Checkpoint Charlie, 43-45, Friedrichstra\u00dfe,' \
      'Mitte, Berlin, 10969, Germany","class":"tourism","type":"attraction",' \
      '"importance":0.5358910050999,"icon":"https:\/\/locationiq.org\/static\/images\/mapicons\/poi_point_of_interest.p.20.png"}]'
    end
    let(:parsed_response) do
      [
        {
          'place_id'=>'3762750',
          'licence'=>'© LocationIQ.com CC BY 4.0,Data © OpenStreetMap contributors, ODbL 1.0',
          'osm_type'=>'node', 'osm_id'=>'417346627',
          'boundingbox'=>['52.5074959', '52.5075959', '13.3903185', '13.3904185'],
          'lat'=>'52.5075459', 'lon'=>'13.3903685',
          'display_name'=>'Checkpoint Charlie, 43-45, Friedrichstraße,Mitte, Berlin, 10969, Germany',
          'class'=>'tourism', 'type'=>'attraction', 'importance'=>0.5358910050999,
          'icon'=>'https://locationiq.org/static/images/mapicons/poi_point_of_interest.p.20.png'
        }
      ]
    end

    describe '#parsed' do
      it 'returns the correct hash' do
        expect(response.parsed).to match(parsed_response)
      end
    end

    describe '#error?' do
      it 'returns false' do
        expect(response.error?).to be_falsy
      end
    end

    describe '#extract_coordinates' do
      let(:extracted_coordinates) do
        { 'lat' => '52.5075459', 'lon' => '13.3903685'}
      end

      it 'returns the correct array' do
        expect(response.extract_coordinates).to match(extracted_coordinates)
      end
    end
  end

  context 'with error' do
    let(:plain_response) { '{"error":"Unable to geocode"}' }
    let(:parsed_response) { { 'error' =>  'Unable to geocode' } }

    describe '#parsed' do
      it 'returns the correct hash' do
        expect(response.parsed).to match(parsed_response)
      end
    end

    describe '#error?' do
      it 'returns true' do
        expect(response.error?).to be_truthy
      end
    end

    describe '#extract_coordinates' do
      it 'raises an error' do
        expect { response.extract_coordinates }
          .to raise_error(LocationIq::CoordinatesNotExtractable)
      end
    end
  end
end
