class GeocodeController < ApplicationController
  before_action :authenticate

  def search
    res = ::LocationIq::Query.new(geocode_params[:q]).search

    if res.error?
      render json: { error: 'unable to geocode' }
    else
      render json: res.extract_coordinates
    end
  end

  private

  def authenticate
    authenticate_or_request_with_http_token do |token, options|
      ActiveSupport::SecurityUtils.secure_compare(
        token,
        Rails.application.credentials.api[:token]
      )
    end
  end

  def geocode_params
    params.permit(:q)
  end
end
