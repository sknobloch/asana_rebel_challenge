# Asana Rebel Challenge

The goal of this exercise is to implement a simple web server application in Ruby that receives an address as a string, and outputs the longitude and latitude of the aforementioned address via HTTP.
The application should include one API endpoint that accepts a GET request, which expects
a query parameter to hold the address query string. The endpoint will have a JSON-formatted response that contains the longitude and latitude of the given address.
For example, if the address query string is “checkpoint charlie”, the API should respond with the longitude and latitude in JSON format.

## Dependencies

- Ruby 2.5.1

## Setup

- `$ bundle install`

## Starting the Server

- `$ bundle exec rails s`

## Example Query

- `$ curl localhost:3000/geocode?q='alexanderplatz' --header "Authorization: Token token=topsecret"`

## Running tests

- `$ bundle exec rspec`

## Guideline Questions

**How do you handle configuration values? What if those values change?**

In Rails 5.2 configuration values like API tokens can be stored safely in `config/credentials.yml.enc`.
Since this file is encrypted, the values can be changed and put under version control like normal code.
The key for decryption is stored in `config/master.key` which normally shouldn't be committed to a repository.
I've added this key anyway for convenience purposes, so it doesn't have to be passed around when evaluating this solution.


Before 5.2 I would have used the [config gem](https://github.com/railsconfig/config) and handle updates of the settings files within the server provisioning process.


**What happens if we encounter an error with the third-party API integration? Will it also break our application, or are they handled accordingly?**

Errors are handled when reading the third-party API response and will result in return in a generic error message to the user.
However, one could distinct a lot more between the different errors they describe in their docs and provide more specific messages to the user!


Other than that I've set short timeouts when requesting the third-party API to prevent server threads from clogging up and ultimately blocking completely.
