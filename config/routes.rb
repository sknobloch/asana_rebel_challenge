Rails.application.routes.draw do
  get 'geocode', to: 'geocode#search'
end
