class LocationIq::Query
  include HTTParty

  base_uri 'https://eu1.locationiq.com'
  default_timeout 2

  attr_accessor :address

  def initialize(address)
    @address = address
  end

  def search
    url = "#{base_path}&q=#{address}"
    response = self.class.get(url)

    LocationIq::Response.new(response.body)
  rescue Net::OpenTimeout, Net::ReadTimeout
    LocationIq::Response.new('{"error": "request timeout"}')
  end

  private

  def base_path
    "/v1/search.php?" \
    "key=#{Rails.application.credentials.location_iq[:token]}" \
    "&format=json"
  end
end
