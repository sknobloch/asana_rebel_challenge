class LocationIq::CoordinatesNotExtractable < StandardError; end

class LocationIq::Response
  attr_accessor :plain_response

  def initialize(plain_response)
    @plain_response = plain_response
  end

  def parsed
    @parsed ||= JSON.parse(plain_response)
  end

  def error?
    parsed.respond_to?(:key?) && parsed.key?('error')
  end

  def extract_coordinates
    raise LocationIq::CoordinatesNotExtractable if error?

    most_relevant.slice('lon', 'lat')
  end

  private

  def most_relevant
    parsed.sort_by { |h| h['importance'] }.reverse.first
  end
end
