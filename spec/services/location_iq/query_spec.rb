RSpec.describe LocationIq::Query, vcr: { record: :new_episodes } do
  let(:address) { 'checkpoint charlie' }
  let(:query) { described_class.new(address) }

  context 'valid address' do
    describe '#search' do
      it 'returns a LocationIq::Response' do
        expect(query.search).to be_an_instance_of(LocationIq::Response)
      end
    end
  end

  context 'invalid address' do
    let(:address) { '' }

    describe '#search' do
      it 'returns a LocationIq::Response' do
        expect(query.search).to be_an_instance_of(LocationIq::Response)
      end
    end
  end

  context 'network timeout' do
    before do
      allow(described_class).to receive(:get).and_raise(Net::OpenTimeout)
    end

    describe '#search' do
      it 'returns a LocationIq::Response' do
        expect(query.search).to be_an_instance_of(LocationIq::Response)
      end
    end
  end
end
