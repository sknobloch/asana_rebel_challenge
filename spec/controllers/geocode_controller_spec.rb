RSpec.describe GeocodeController, vcr: { record: :new_episodes } do
  let(:params) { {} }
  let(:auth_header) do
    "Token token=#{Rails.application.credentials.api[:token]}"
  end

  before do
    request.headers['Authorization'] = auth_header
    get :search, params: params
  end

  context 'authentication' do
    context 'given a valid token' do
      describe 'GET search' do
        it 'should grant access' do
          expect(response.status).to eq(200)
        end
      end
    end

    context 'given an invalid token' do
      let(:auth_header) { 'invalid' }

      describe 'GET search' do
        it 'should deny access' do
          expect(response.status).to eq(401)
        end
      end
    end
  end

  context 'without errors' do
    let(:params) { { q: 'checkpoint charlie' } }
    let(:expected_response) { { 'lat' => '52.5075459', 'lon' => '13.3903685' } }

    describe 'GET search' do
      it 'renders JSON' do
        expect(response.content_type).to eq('application/json')
      end

      it 'responds with the correct coordinates' do
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end

  context 'with errors' do
    let(:params) { { q: '' } }
    let(:expected_response) { { 'error' => 'unable to geocode' } }

    describe 'GET search' do
      it 'renders JSON' do
        expect(response.content_type).to eq('application/json')
      end

      it 'responds with the correct coordinates' do
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end
end
